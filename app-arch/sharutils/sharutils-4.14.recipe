SUMMARY="sharutils"
DESCRIPTION="
GNU shar makes so-called shell archives out of many files, preparing them for \
transmission by electronic mail services. A shell archive is a collection of \
files that can be unpacked by /bin/sh. A wide range of features provide \
extensive flexibility in manufacturing shars and in specifying shar smartness. \
For example, shar may compress files, uuencode binary files, split long files \
and construct multi-part mailings, ensure correct unsharing order, and provide \
simplistic checksums.
GNU unshar scans a set of mail messages looking for the start of shell \
archives. It will automatically strip off the mail headers and other \
introductory text. The archive bodies are then unpacked by a copy of the \
shell. unshar may also process files containing concatenated shell archives.
"
HOMEPAGE="http://www.gnu.org/software/sharutils/"
LICENSE="GNU GPL v3"
COPYRIGHT="1990-2013 Free Software Foundation, Inc."
SRC_URI="http://ftp.gnu.org/gnu/sharutils/sharutils-4.14.tar.gz"
CHECKSUM_SHA256="90f5107c167cfd1b299bb211828d2586471087863dbed698f53109cd5f717208"
REVISION="1"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	sharutils$secondaryArchSuffix = $portVersion
	cmd:shar$secondaryArchSuffix = $portVersion
	cmd:unshar$secondaryArchSuffix = $portVersion
	cmd:uudecode$secondaryArchSuffix = $portVersion
	cmd:uuencode$secondaryArchSuffix = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	"

BUILD_PREREQUIRES="
	cmd:awk
	cmd:make
	cmd:which
	cmd:bison
	cmd:makeinfo
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	"

PATCHES="sharutils-4.14.patchset"

BUILD()
{
	runConfigure ./configure \
	--disable-nls
	make
}

INSTALL()
{
	make install
	rm -rf $libDir
}
